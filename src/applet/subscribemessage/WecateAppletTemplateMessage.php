<?php
namespace Maowenke\PersonalWechatSdk\applet\subscribemessage;
use Maowenke\PersonalWechatSdk\applet\WecateApplet;

class WecateAppletTemplateMessage extends WecateApplet
{
    /**获取帐号所属类目下的公共模板标题
     * @param string $ids
     */
    public function getPubTemplateTitleList(string $ids=''){
        $access_token = $this->getAppletAccessToken();
        if(!$access_token){
            $this->message = '获取access_token失败';
            return false;
        }
        $url = $this->url."wxaapi/newtmpl/getpubtemplatetitles?access_token={$access_token}&ids={$ids}";
        $start = 0;
        $limit = 30;
        $data = [];
        while (true){
//            if($start>=3){
//                break;
//            }
            $starts = $start*$limit;
            $urls = $url."&start={$starts}&limit={$limit}";
            $array = $this->curl->http_requests($urls,[],[],"GET");
            if(!$array){
                $this->message = $array['errmsg']??'未返回数据';
                return false;
            }
            if(isset($array['errcode'])&&$array['errcode']==0){
                if(is_array($array['data'])&&count($array['data'])>0){
                    $data = array_merge($data,$array['data']);
                    $start++;
                }else{
                    break;
                }
            }else{
                $this->message = $array['errmsg']??'未返回数据';
                return false;
            }
        }
        return $data;
    }

    /**获取模板标题下的关键词列表
     * @param string $tid
     */
    public function getPubTemplateKeyWordsById(string $tid=''){
        $access_token = $this->getAppletAccessToken();
        if(!$access_token){
            $this->message = '获取access_token失败';
            return false;
        }
        $url = $this->url."wxaapi/newtmpl/getpubtemplatekeywords?access_token={$access_token}&tid={$tid}";
        $array = $this->curl->http_requests($url,[],[],'GET');
        if(isset($array['errcode'])&&$array['errcode']==0){
            return $array['data'];
        }else{
            $this->message = $array['errmsg']??'未返回数据';
            return false;
        }
    }

    /**组合模板并添加至帐号下的个人模板库
     * @param string|模板标题id $tid
     * @param array|开发者自行组合好的模板关键词列表 $kidList
     * @param string|服务场景描述，15个字以内 $sceneDesc
     * @return bool|mixed|string
     */
    public function addTemplate(string $tid,array $kidList,string $sceneDesc = ''){
        $access_token = $this->getAppletAccessToken();
        if(!$access_token){
            $this->message = '获取access_token失败';
            return false;
        }
        $url = $this->url."wxaapi/newtmpl/addtemplate?access_token={$access_token}";
        $data = [
            'tid'=>$tid,
            'kidList'=>$kidList,
            'sceneDesc'=>$sceneDesc,
        ];
        $array = $this->curl->http_requests($url,$data,['content-type: application/json'],"POST");
        if(isset($array['errcode'])&&$array['errcode']==0){
            return $array;
        }else{
            $this->message = $array['errmsg']??'未返回数据';
            return false;
        }
    }

    /**删除帐号下的个人模板
     * @param  string|要删除的模板id $priTmplId
     * @return bool
     */
    public function deleteTemplate(string $priTmplId){
        $access_token = $this->getAppletAccessToken();
        if(!$access_token){
            $this->message = '获取access_token失败';
            return false;
        }
        $url = $this->url."wxaapi/newtmpl/deltemplate?access_token={$access_token}";
        $array = $this->curl->http_requests($url,['priTmplId'=>$priTmplId],['content-type: application/json']);
        if(isset($array['errcode'])&&$array['errcode']==0){
            return true;
        }else{
            $this->message = $array['errmsg']??'未返回数据';
            return false;
        }
    }

    /**获取当前帐号下的个人模板列表
     * @return false|mixed
     */
    public function getTemplateList(){
        $access_token = $this->getAppletAccessToken();
        if(!$access_token){
            $this->message = '获取access_token失败';
            return false;
        }
        $url = $this->url."wxaapi/newtmpl/gettemplate?access_token={$access_token}";
        $array = $this->curl->http_requests($url,[],[],"GET");
        if(isset($array['errcode'])&&$array['errcode']==0){
            return $array['data'];
        }else{
            $this->message = $array['errmsg']??'未返回数据';
            return false;
        }
    }

    /**发送订阅消息
     * @param string|接收者 $touser
     * @param string|模板id $template_id
     * @param array|模板内容 $data
     * @param string|跳转页面 $page
     * @param string|跳转小程序类型 $miniprogram_state
     * @param string|语言类型 $lang
     * @return bool
     */
    public function send(string $touser,string $template_id,array $data=[],string $page='',string $miniprogram_state='formal',string $lang='zh_CN'){
        $access_token = $this->getAppletAccessToken();
        if(!$access_token){
            $this->message = '获取access_token失败';
            return false;
        }
        $url = $this->url."cgi-bin/message/subscribe/send?access_token={$access_token}";
        $newdata = [
            'access_token'=>$access_token,
            'touser'=>$touser,
            'template_id'=>$template_id,
            'page'=>$page,
            'data'=>$data,
            'miniprogram_state'=>$miniprogram_state,
            'lang'=>$lang,
        ];
        $array = $this->curl->http_requests($url,$newdata,['content-type: application/json']);
        if(isset($array['errcode'])&&$array['errcode']==0){
            return true;
        }else{
            $this->message = $array['errmsg']??'未返回数据';
            return false;
        }
    }

    /**统一服务消息
     * @param $touser
     * @param array $mp_template_msg
     * @return false
     */
    public function sendUniformMessage($touser,$mp_template_msg = []){
        $access_token = $this->getAppletAccessToken();
        if(!$access_token){
            $this->message = '获取access_token失败';
            return false;
        }
        $url = $this->url."cgi-bin/message/wxopen/template/uniform_send?access_token={$access_token}";
        $newdata = [
            'access_token'=>$access_token,
            'touser'=>$touser,
            'mp_template_msg'=>$mp_template_msg,
        ];
        $array = $this->curl->http_requests($url,$newdata,['content-type: application/json']);
        if(isset($array['errcode'])&&$array['errcode']==0){

        }else{
            if(isset($array['errcode'])){
                $this->message = $array['errmsg']??'未返回数据';
            }else{
                $this->message = '网络通信失败';
            }
            return false;
        }
    }
}