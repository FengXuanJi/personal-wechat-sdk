<?php
namespace Maowenke\PersonalWechatSdk;
class Curl
{
    protected $message = '';
    protected $sslCertPath = '';
    protected $sslKeyPath = '';
    public function __construct($config = []){
        if(!empty($config)){
            if(is_array($config)){
                foreach ($config as $key=>$value){
                    $this->$key = $value;
                }
            }
        }
    }
    public function getMessage(){
        return $this->message;
    }
    public function http_requests(string $url,$data,array $header= [],$method='POST',$useCert=false,$json=true){
        $oCurl = curl_init();

        curl_setopt($oCurl, CURLOPT_URL, $url);

        curl_setopt($oCurl, CURLOPT_HTTPHEADER, $header);

//关闭https验证
        if($useCert){
            curl_setopt($oCurl,CURLOPT_SSL_VERIFYPEER,TRUE);
            curl_setopt($oCurl,CURLOPT_SSL_VERIFYHOST,2);//严格校验
            curl_setopt($oCurl,CURLOPT_SSLCERTTYPE,'PEM');
            curl_setopt($oCurl,CURLOPT_SSLCERT, $this->sslCertPath);
            curl_setopt($oCurl,CURLOPT_SSLKEYTYPE,'PEM');
            curl_setopt($oCurl,CURLOPT_SSLKEY, $this->sslKeyPath);
        }else{
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
        }

//        $data = json_encode( $data ,JSON_UNESCAPED_UNICODE );
        if(strtoupper($method)=="POST"){
            curl_setopt($oCurl,CURLOPT_POST,true);
            curl_setopt($oCurl, CURLOPT_CUSTOMREQUEST, 'POST');
        }elseif (strtoupper($method)=="PUT"){
            curl_setopt($oCurl, CURLOPT_CUSTOMREQUEST, 'PUT');
        }elseif (strtoupper($method)=="DELETE"){
            curl_setopt($oCurl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        }elseif (strtoupper($method)=="GET"){
            curl_setopt($oCurl, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($oCurl,CURLOPT_POST,false);
        }
        if(!empty($data)){
            if(is_array($data)){
                curl_setopt($oCurl,CURLOPT_POSTFIELDS,http_build_query($data));
            }else{
                curl_setopt($oCurl,CURLOPT_POSTFIELDS,$data);
            }
//            curl_setopt($oCurl,CURLOPT_POSTFIELDS,$data);
        }


//至关重要，CURLINFO_HEADER_OUT选项可以拿到请求头信息

        curl_setopt($oCurl, CURLINFO_HEADER_OUT, TRUE);

        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);

//curl_setopt($oCurl, CURLOPT_POSTFIELDS, $bodystr);

        $sContent = curl_exec($oCurl);
//通过curl_getinfo()可以得到请求头的信息

        $a=curl_getinfo($oCurl);
        if($error=curl_errno($oCurl)){
            $this->message = $error;
            return false;
        }
        if($json){
            try{
                $sContent = json_decode($sContent,true);
            }catch (\Exception $e){
                return $sContent;
            }
            return $sContent;
        }else{
            return $sContent;
        }


    }
    public function xml_http($url,$xml,$useCert=false,$second = 30){
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);

        curl_setopt($ch,CURLOPT_URL, $url);
//        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,TRUE);
//        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
//        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);//严格校验
//        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,FALSE);//严格校验
        if($useCert){
            curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,TRUE);
            curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);//严格校验
            curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM');
            curl_setopt($ch,CURLOPT_SSLCERT, $this->sslCertPath);
            curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
            curl_setopt($ch,CURLOPT_SSLKEY, $this->sslKeyPath);
        }else{
            curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
            curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,FALSE);//严格校验
        }
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        //post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        //运行curl
        $data = curl_exec($ch);
        //返回结果
        if($data){
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);
            curl_close($ch);
            die("curl出错，错误码:$error");
        }
    }
}