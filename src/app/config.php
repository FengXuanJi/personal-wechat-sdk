<?php
return [
    'appid'=>'**********',
    'secret'=>'**********',
    'redirect_uri'=>'**********',
    'scope'=>'snsapi_base|snsapi_userinfo',
    'response_type'=>'code',
    'state'=>'123456',
    'mch_id'=>'**********',//使用支付时使用
    'trade_type'=>'app',//支付方式
    'mch_key'=>'*****',//支付的秘钥
];